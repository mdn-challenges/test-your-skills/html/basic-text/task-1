# MDN Challenges

## Test Your Skills

### HTML - Basic Text

---

#### **Problem Statement**

In this task, mark up the provided HTML using semantic heading and paragraph elements.

The finished example should look like this:  

![task output image](res/images/task-output.png)

---

#### **Concept Page**

[HTML Text Fundamentals](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/HTML_text_fundamentals)


---

[**Original Challenge**](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Test_your_skills:_HTML_text_basics#task_1)

---

> All credits to [MDN](https://developer.mozilla.org/en-US/) for creating this challenge.  
> I have just created a structured version on GitLab for my students to use.